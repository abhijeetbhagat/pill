#pragma once
#include <memory>
#include <map>
#include <stack>
#include <llvm/IR/Module.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Type.h>
#include "block.h"
#include "module.h"

class PModule;

class CodeGenBlock{
 public:
  std::unique_ptr<llvm::BasicBlock> block;
  std::map<std::string, llvm::Value*> locals;
};

class CodeGenContext{
 public:
  llvm::Module *module;

  CodeGenContext(){
    module = new llvm::Module("main", llvm::getGlobalContext());
  }

  void generate_code(PModule *root){
    
  }

  std::map<std::string, llvm::Value*> locals(){
    return blocks.top()->locals;
  }

  std::unique_ptr<llvm::BasicBlock> current_block(){
    return std::move(blocks.top()->block);
  }

  void push_block(std::unique_ptr<llvm::BasicBlock> block){
    blocks.push(std::move(std::unique_ptr<CodeGenBlock>(new CodeGenBlock())));
    blocks.top()->block = std::move(block);
  }

 private:  
  std::stack<std::unique_ptr<CodeGenBlock>> blocks;
  llvm::Function *mainFunction;
};
