#pragma once
#include <memory>
#include <cstdlib>
#include "lexer.h"
#include "token.h"
#include "module.h"
#include "declarations.h"
#include "type.h"
#include "expressions.h"

#define PRINT_ERROR(message) std::cout << '\n' << lexer->token.line_no << ": " << message; \
  std::exit(EXIT_FAILURE);

class Parser{
 public:
  Parser(std::string src);

  TokenType peek();
  void parse();
  std::unique_ptr<FunctionDeclaration> parse_function();
  std::unique_ptr<VarDeclaration> parse_var_decl(PType var_type);
  std::unique_ptr<Expression> parse_expr();
  bool is_type(const TokenType & token);
  bool is_operator(const TokenType & token);
  Operator get_op(TokenType op_token);
  void generate_code(CodeGenContext & context);

 private:
  std::unique_ptr<Lexer> lexer;
  std::unique_ptr<PModule> root_module;
};
