#include "parser.h"
#include <iostream>

Parser::Parser(std::string src) : lexer(new Lexer(src)){ }

TokenType Parser::peek(){
  
}

void Parser::parse(){
  TokenType tok_type;
  while((tok_type = lexer->get_token())!= TOK_EOF){
    switch(tok_type){
    case TOK_INT:
      std::cout << "int found\n";
      {
	PType var_type;
	var_type.kind = INT;
	std::unique_ptr<VarDeclaration> var_decl(parse_var_decl(var_type));
	root_module->add(std::move(var_decl));
      }
      break;
    case TOK_IDENT: 
      std::cout << "identifier found " << lexer->token.identifier->get_identifier() << '\n';
      break;
    case TOK_SEMICOLON:
      std::cout << "semicolon found\n";
      break;
    case TOK_MODULE:
      std::cout << "module found";
      if(lexer->get_token() == TOK_IDENT){

	if(lexer->get_token() == TOK_SEMICOLON){
	  root_module = std::unique_ptr<PModule>(new PModule(lexer->token.identifier->get_identifier()));
	  std::cout << "module name " << lexer->token.identifier->get_identifier() << '\n';
	}
	else{
	  PRINT_ERROR("; expected after module name\n")
	    EXIT_SUCCESS;
	} 
      }
      else{
	PRINT_ERROR("module name expected\n")
	  }
		
      break;
    case TOK_FN:
      {
	std::unique_ptr<FunctionDeclaration> function_decl( parse_function());
	root_module->add(std::move(function_decl));
      }
      break;
    default:
      return;
      std::cout << "undefined token\n";
    }
  }//while ends
  // for(auto cur_tok = token_list.begin(), tok_ptr = cur_tok; 
  // 	cur_tok != token_list.end(); 
  // 	++cur_tok){
  //   switch(cur_tok.first){
  //   case INT:
  // 	//this could be an identifier decl or a function decl; so peek
  // 	if(){
  // 	} 
  //   }
  // }//for ends
} //parse ends

std::unique_ptr<FunctionDeclaration> Parser::parse_function(){
  //(new FunctionDeclaration());
  Identifier *function_name;
  std::unique_ptr<FunctionDeclaration> function_decl;
  if(lexer->get_token() == TOK_IDENT){
    function_name = lexer->token.identifier;
    if(lexer->get_token() == TOK_OPEN_SQUARE_BRACE){
      bool type_expected = true;
      bool is_signature_over = false;
      TokenType next_token = lexer->get_token();
      while(next_token != TOK_CLOSE_SQUARE_BRACE){
	if(type_expected){
	  if(is_type(next_token)){//(next_token == TOK_INT){
	    //store type here
	  }
	  else{
	    PRINT_ERROR("type expected\n") 
	      }
	}
	else{
	  if(!(next_token == TOK_COMMA)){
	    PRINT_ERROR(", expected after type\n");
	    type_expected = !type_expected;
	  }
	}
	next_token = lexer->get_token();
	type_expected = !type_expected;
      }
      if(type_expected){ //trailing comma found
	PRINT_ERROR("type expected after comma\n")
	  }

      if(lexer->get_token() == TOK_OPEN_CURLY_BRACE &&
	 lexer->get_token() == TOK_CLOSE_CURLY_BRACE){
	std::cout << "function def found\n";
      }
      else{
	PRINT_ERROR("{ expected after function signature")
	  }
    }
    else{
      PRINT_ERROR("[ expected after function name\n")
	}
  }
  else{
    PRINT_ERROR("function name expected\n")
      EXIT_SUCCESS;
  }
  PType return_type;
  return_type.kind = INT;
  function_decl = std::unique_ptr<FunctionDeclaration>(new FunctionDeclaration(function_name,
									       return_type,
									       nullptr
									       ));
  return function_decl;
}

std::unique_ptr<VarDeclaration> Parser::parse_var_decl(PType var_type){
  std::unique_ptr<VarDeclaration> var_decl;
  if(lexer->get_token() == TOK_IDENT){
    std::cout << "identifier found\n";
    Identifier *var_name = lexer->token.identifier;
    TokenType next_token = lexer->get_token();
    switch(next_token){
    case TOK_SEMICOLON:
      var_decl = std::unique_ptr<VarDeclaration>(new VarDeclaration(var_name, var_type, nullptr));
	
      break;
    case TOK_ASSIGN:
      {
	std::cout << "assign found\n";
	//expression: could be another var, lit or a function-call
	return std::unique_ptr<VarDeclaration>(new VarDeclaration(var_name,
								  var_type,
								  std::move(parse_expr())));
	break;
      }
    default:
      PRINT_ERROR("; or = expected\n")
	}
  }
  else{
    PRINT_ERROR("identifier expected after type\n");
  }
  return var_decl;
}

//TODO refactor the common part
std::unique_ptr<Expression> Parser::parse_expr(){
  std::unique_ptr<Expression> expr;
  switch(lexer->get_token()){
  case TOK_INTLIT:
    {
      std::unique_ptr<IntegerExpression> int_expr(new IntegerExpression(lexer->token._int));
      TokenType next_token = lexer->get_token();
      if(is_operator(next_token)){
	return std::unique_ptr<BinaryExpression>(new BinaryExpression(get_op(next_token),
								      std::move(int_expr),
								      parse_expr()));
      }
      else if(next_token == TOK_SEMICOLON){
	return std::move(int_expr);
      }
      else{
	PRINT_ERROR("an operator or a semicolon expected\n")
	  }
      //expression: could be another var, lit or a function-call 
    }
  case TOK_IDENT:
    {
      std::unique_ptr<Identifier> id(lexer->token.identifier);
      std::unique_ptr<IdentifierExpression> ident_expr(new IdentifierExpression(std::move(id)));
      TokenType next_token = lexer->get_token();
      if(is_operator(next_token)){
	return std::unique_ptr<BinaryExpression>(new BinaryExpression(get_op(next_token),
								      std::move(ident_expr),
								      parse_expr()));
      }
      else if(next_token == TOK_SEMICOLON){
	return std::move(ident_expr);
      }
      else{
	PRINT_ERROR("an operator or a semicolon expected\n")
	  }
    }
  default:
    break; 
  }
  
  return expr;
}

bool Parser::is_type(const TokenType & token){
  switch(token){
  case TOK_INT:
    return true;
  default:
    return false;
  }
}

bool Parser::is_operator(const TokenType & token){
  switch(token){
  case TOK_PLUS:
  case TOK_MINUS:
  case TOK_ASTERISK:
  case TOK_FORWARD_SLASH:
    return true;
  default:
    return false;
  }
}

Operator Parser::get_op(TokenType op_token){
  switch(op_token){
  case TOK_PLUS:
    return OP_PLUS;
  case TOK_MINUS:
    return OP_MINUS;
  case TOK_ASTERISK:
    return OP_MUL;
  case TOK_FORWARD_SLASH:
    return OP_DIV; 
  }
}

void Parser::generate_code(CodeGenContext & context){
  root_module->code_gen(context);
}
