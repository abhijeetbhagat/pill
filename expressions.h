#pragma once

#include <memory>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Value.h>
#include <llvm/IR/Instructions.h>
#include "codegen.h"
#include "node.h"

enum Operator{
  OP_PLUS,
  OP_MINUS,
  OP_MUL,
  OP_DIV,
  OP_MOD
};

class Identifier;

class Expression : public Node {};

class IntegerExpression : public Expression{
 public:
  IntegerExpression(int value);
  
  llvm::Value* code_gen(CodeGenContext & context);

 private:
  int _value;
};

class IdentifierExpression : public Expression{
 public:
  IdentifierExpression(std::unique_ptr<Identifier> identifier);
  llvm::Value* code_gen(CodeGenContext & context);

 private:
  std::unique_ptr<Identifier> _id;
};

class BinaryExpression : public Expression{
 public:
 BinaryExpression(Operator op,
		  std::unique_ptr<Expression> left,
		  std::unique_ptr<Expression> right);

  llvm::Value* code_gen(CodeGenContext & context);
 private:
  Operator _op;
  std::unique_ptr<Expression> left_expr;
  std::unique_ptr<Expression> right_expr;
};
