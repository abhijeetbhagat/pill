/* clang++-3.6 -std=c++11 lexer.cpp `llvm-config-3.6 --cxxflags --libs core jit native --ldflags` -ltinfo -o lexer

  clang++-3.6 -std=c++14 main.cpp `llvm-config-3.6 --cxxflags --ldflags --system-libs --libs core` -o pillc
 */

#include <iostream>
#include <string>
#include <sstream>
#include <fstream>
#include "llvm/IR/Verifier.h"
#include "llvm/IR/DerivedTypes.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/LLVMContext.h"
#include "llvm/IR/Module.h"
#include "parser.h"
#include <memory>
//#include "codegen.h"
//#include "module.h"

int main(){
  std::ifstream file{"test.p"};
  std::stringstream buffer;
  buffer << file.rdbuf();

  //Parser *parser = new Parser(buffer.str());
  std::unique_ptr<Parser> parser(new Parser(buffer.str()));
  parser->parse();
  parser->generate_code();
  //delete parser;
  // src = buffer.str();
  // c = src.begin();
  // while(c!=src.end()){
  //   token_list.push_back(get_token());
  //   std::cout << p.first << std::endl;
  // }
  
}
