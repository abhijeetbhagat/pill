#include <cctype>
#include <string>
#include "token.h"

#pragma once

class Lexer{
public: 
  Lexer(std::string src);
  
  TokenType get_token();
  
  Token token;
  
private:
  std::string _src;
  std::string::iterator c; 
};
