#include "expressions.h"

IntegerExpression::IntegerExpression(int value) : _value(value){}
  
llvm::Value* IntegerExpression::code_gen(CodeGenContext & context){
  return llvm::ConstantInt::get(llvm::getGlobalContext(), llvm::APInt(32, _value));
}


IdentifierExpression::IdentifierExpression(std::unique_ptr<Identifier> identifier) :
  _id(std::move(identifier)){}

llvm::Value* IdentifierExpression::code_gen(CodeGenContext & context){
  return new llvm::LoadInst(context.locals()[_id->get_identifier()],
			    "",
			    false,
			    context.current_block().get());
}


BinaryExpression::BinaryExpression(Operator op,
				   std::unique_ptr<Expression> left,
				   std::unique_ptr<Expression> right):
  _op(op),
  left_expr(std::move(left)),
  right_expr(std::move(right)){}

llvm::Value* BinaryExpression::code_gen(CodeGenContext & context){
  return nullptr;
}
