#pragma once

#include "statements.h"
#include "node.h"


class Block{
 public:
  void add(Statement *statement){
    _statements.push_back(statement);
  }

  ~Block(){
    for(auto s : _statements){
      delete s;
    }
  }

  llvm::Value *codeGen(){
    return nullptr;
  }
 private:
  std::vector<Statement*> _statements;
  
};
