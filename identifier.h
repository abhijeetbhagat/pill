#pragma once

struct Identifier{
  Identifier(std::string value) : _value(value){}
  
  std::string & get_identifier(){
    return _value;
  }
  
private:
  std::string _value;
};
