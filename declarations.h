#pragma once

#include <memory>
#include <llvm/IR/Value.h>
#include "expressions.h"
#include "type.h"
#include "identifier.h"
#include "codegen.h"
#include "node.h"

class Expression;

class Declaration : public Expression{};

class VarDeclaration : public Declaration{
 public:
  VarDeclaration(Identifier *var_name,
		 PType var_type,
		 std::unique_ptr<Expression> initializer);
  llvm::Value* code_gen(CodeGenContext & context);

 private:
  Identifier *_var_name;
  PType _var_type;
  std::unique_ptr<Expression> expr;
};

typedef std::vector<VarDeclaration*> VarDeclarationList;

class FunctionDeclaration : public Declaration{
 public:
  FunctionDeclaration(Identifier *func_name,
		      PType return_type,
		      VarDeclarationList *arg_list);

  llvm::Value* code_gen(CodeGenContext & context);
  
 private:
  Identifier *_func_name;
  PType _return_type;
  VarDeclarationList *_args;
};

