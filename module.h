#pragma once

#include <memory>
#include <vector>
#include "declarations.h"
#include "codegen.h"
#include "node.h"

class Declaration;

class PModule : public Node{
 public:
  PModule(const std::string & name);

  void add(std::unique_ptr<Declaration> declaration);

  llvm::Value *code_gen(CodeGenContext & context);
 
 private:
  std::string _name;
  std::vector<std::unique_ptr<Declaration>> declarations;
};
