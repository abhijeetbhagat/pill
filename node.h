#pragma once

#include <llvm/IR/Value.h>
#include "codegen.h"
class CodeGenContext;
class Node{
 public:
  virtual llvm::Value *code_gen(CodeGenContext & context) = 0;
};
