#pragma once


enum TypeKind{
  INT,
  VOID,
  CLASS
};

struct PType{
  TypeKind kind;
  std::string class_name;
};
