#include "lexer.h"

Lexer::Lexer(std::string src) : _src(src), c(_src.begin()) { }
  
TokenType Lexer::get_token(){
    std::string token_str = "";
    if(c == _src.end()){
      return TOK_EOF;
    }
    if(std::isspace(*c)){
      c++;
      return get_token();
    }
    if(std::isalnum(*c)){
      if(std::isdigit(*c)){ //nums
	do{
	  token_str += *c;
	  c++;
	}while(std::isdigit(*c));//while(!std::isspace(*c));
	if(std::isspace(*c))
	  c++;
	token._int = std::stoi(token_str);
	return TOK_INTLIT;
      }
      else { //identifiers
	do{
	  token_str += *c;
	  c++;
	}while( std::isalpha(*c));
	if(std::isspace(*c))
	  c++;
	
	if(token_str == "int"){
	  return TOK_INT;
	}
	else if(token_str == "if"){
	  return TOK_IF;
	}
	else if(token_str == "for"){
	  return TOK_FOR;
	}
	else if(token_str == "module"){
	  return TOK_MODULE;
	}
	else if(token_str == "fn"){
	  return TOK_FN;
	}
	else{
	  token.identifier = new Identifier(token_str);
	  return TOK_IDENT;
	}
      }
    }
    else{
      switch(*c++){ //special chars
      case '(':
	return TOK_OPEN_RND_BRACE;
      case ')':
	return TOK_CLOSE_RND_BRACE;
      case '{':
	return TOK_OPEN_CURLY_BRACE;
      case '}':
	return TOK_CLOSE_CURLY_BRACE;
      case '[':
	return TOK_OPEN_SQUARE_BRACE;
      case ']':
	return TOK_CLOSE_SQUARE_BRACE;
      case ',':
	return TOK_COMMA;
      case '\'':
	return TOK_SINGLE_QUOTE;
      case '"':
	return TOK_DOUBLE_QUOTE;
      case ';': 
	return TOK_SEMICOLON;
      case '+':
	return TOK_PLUS;
      case '-':
	return TOK_MINUS;
      case '*':
	return TOK_ASTERISK;
      case '/':
	return TOK_FORWARD_SLASH;
      case '=':
	return TOK_ASSIGN;
      case '\n':
	token.line_no++;
	return get_token();
      }
    }
    return TOK_OPEN_CURLY_BRACE;
  }
