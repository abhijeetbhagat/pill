#include "module.h"

PModule::PModule(const std::string & name) : _name(name){}

void PModule::add(std::unique_ptr<Declaration> declaration){
  declarations.push_back(std::move(declaration));
}

llvm::Value* PModule::code_gen(CodeGenContext & context){
  for(auto const & declaration : declarations){
    declaration->code_gen(context);
  }
  return nullptr;
}
