lflags += -std=c++14 `llvm-config-3.6 --cxxflags --ldflags --system-libs --libs core native`
CC = clang++-3.6
SRC = lexer.cpp lexer.h parser.cpp parser.h token.h main.cpp \
	expressions.cpp expressions.h \
	declarations.cpp declarations.h \
	module.cpp module.h

OBJS = lexer.o parser.o main.o \
	expressions.o declarations.o module.o

binary = pillc

$(OBJS): %.o : %.cpp
	$(CC) $(lflags) -c $< -o $@

all: $(binary)

frontend.a: $(OBJS)
	ar rcs frontend.a $(OBJS)

$(binary): frontend.a
	$(CC) frontend.a $(lflags) -o $(binary)

clean:
	rm *.o $(binary) *.a
