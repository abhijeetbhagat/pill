#pragma once

#include <memory>
#include "identifier.h"

enum TokenType{
  TOK_INTLIT,
  TOK_FLOATLIT,
  TOK_IDENT,
  TOK_STRINGLIT,
  TOK_DOUBLE_QUOTE,
  TOK_SINGLE_QUOTE,
  TOK_OPEN_RND_BRACE,
  TOK_CLOSE_RND_BRACE,
  TOK_OPEN_CURLY_BRACE,
  TOK_CLOSE_CURLY_BRACE,
  TOK_OPEN_SQUARE_BRACE,
  TOK_CLOSE_SQUARE_BRACE,
  TOK_COMMA,
  TOK_FOR,

  //Types
  TOK_INT,
  TOK_BOOL,
  //////////

  TOK_IF,
  TOK_EOF,
  TOK_SEMICOLON,
  TOK_ASSIGN,

  //Operators
  TOK_PLUS,
  TOK_MINUS,
  TOK_ASTERISK,
  TOK_FORWARD_SLASH,
  //////////////////

  TOK_MODULE,
  TOK_FN
};

struct Token{
  Token(){}
  uint line_no;
  TokenType token_type;
  union{
    int _int;
    Identifier *identifier;
    std::string *_string;
  };
};
