#include "declarations.h"

VarDeclaration::VarDeclaration(Identifier *var_name,
			       PType var_type,
			       std::unique_ptr<Expression> initializer) :
  _var_name(var_name),
  _var_type(var_type),
  expr(std::move(initializer)){}

llvm::Value* VarDeclaration::code_gen(CodeGenContext & context){
  return nullptr;
}


FunctionDeclaration::FunctionDeclaration(Identifier *func_name,
					 PType return_type,
					 VarDeclarationList *arg_list) : 
  _func_name(func_name),
  _return_type(return_type),
  _args(arg_list){}

llvm::Value* FunctionDeclaration::code_gen(CodeGenContext & context){
  return nullptr;
}
